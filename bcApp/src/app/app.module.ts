import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { WorkfinishPage } from '../pages/workfinish/workfinish';
import { PicIniPage } from '../pages/pic-ini/pic-ini';
import { PicFinishPage } from '../pages/pic-finish/pic-finish';
import { AddWorkPage } from '../pages/add-work/add-work';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';

// Import Firebase / environment config and initialise
import * as firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth'
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { Preloader } from '../providers/preloader/preloader';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { WorksProvider } from '../providers/works/works';
import { CaptureProvider } from '../providers/capture/capture';
//pipes
import { WorksStatusPipe } from '../pipes/works-status/works-status';


firebase.initializeApp(environment.firebase);


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    WorksStatusPipe,
    WorkfinishPage,
    PicIniPage,
    PicFinishPage,
    AddWorkPage
  ],
  imports: [
    BrowserModule,
    HttpModule,    
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    WorkfinishPage,
    PicIniPage,
    PicFinishPage,
    AddWorkPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    Preloader,
    FirebaseProvider,
    WorksProvider,
    CaptureProvider
  ]
})
export class AppModule {}
