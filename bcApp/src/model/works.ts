export class Works{
	id: number;  
	actividad: string[];
	property: string;
	propertyno:string;
	dataproperty:Object[];
	type: string;
	created: any;
	sort:any;
	photos:Object[];
	status:string;
	pics_ini:Object[];
	pics_finish:Object[];
	  
	constructor(values: Object = {}) {
	   Object.assign(this, values);
	}
}
export enum WorksStatus {
    DEFAULT,
    PENDING,
    ACCEPTED,
    STARTED,
    ONIT,
    FINISH
}