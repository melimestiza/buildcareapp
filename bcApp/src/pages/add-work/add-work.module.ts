import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddWorkPage } from './add-work';

@NgModule({
  declarations: [
    AddWorkPage,
  ],
  imports: [
    IonicPageModule.forChild(AddWorkPage),
  ],
})
export class AddWorkPageModule {}
