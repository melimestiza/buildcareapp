import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { Works,WorksStatus } from '../../model/works';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { WorksProvider } from '../../providers/works/works';
import { Observable } from 'rxjs/Observable';
import { CaptureProvider } from '../../providers/capture/capture';
import { Preloader } from '../../providers/preloader/preloader';
import { HomePage } from '../home/home';


/**
 * Generated class for the AddWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-work',
  templateUrl: 'add-work.html',
})
export class AddWorkPage {
	private workForm : FormGroup;
  public properties:Object;
  private works:Works;
  public photosAdded:Boolean;
  public photos:string[];
  public property:string[];
  private maxPictures:Number;






  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder,public wp: WorksProvider,private _IMG: CaptureProvider,private alertCtrl : AlertController,private _LOADER: Preloader) {
    this.navCtrl = navCtrl;
    this.works = new Works();
    this.photosAdded = false;
    this.photos = [];
    this.property = [];
    this.works.photos = [];
    this.maxPictures = 1;


    console.log(this.works);

//    this.properties = wp.getAllProperties();
    this.wp.getAllProperties().subscribe( dataObject => {
          this.properties = dataObject;
          console.log(this.properties);
        });

  	this.workForm = this.formBuilder.group({
      property: ['',Validators.required],
      noapto: '',
      photosAdded: ['', Validators.compose([AddWorkPage.isChecked, Validators.required])],

 
    });
    this.photos.push("data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==");
    this.photosAdded = (this.photos.length > 0)? true : false;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AddWorkPage');
  }
  save(){
    if(this.works.property == null)
      this.works.property = "";
    if(this.works.propertyno == null)
      this.works.propertyno = "";

    this.property['property'] = this.works.property;
    this.property['propertyno'] = this.works.propertyno;
    this.property['photo'] = this.photos;
    this.works.dataproperty = this.property;
    this.works.status = WorksStatus.PENDING.toString();       
      this._LOADER.displayPreloader('Procesando...');        
      this.wp.save(this.works).then(registerData => {
        console.log(registerData);
        this._LOADER.hidePreloader();
        this.cancel();
      }, registerError => {
        console.log(registerError);
        this._LOADER.hidePreloader();
      });
   
  }
  setProperty(){
    console.log(this.works.property);
    //this.works.property = selectComponent._text;
    return false;  
  }
  public selectObjectById(list: any[], id: string, property: string) {
    if (id) {
      var obj = list.find(item => item.key === id);
      this.property['name'] = obj.value.name;
    }
    
}

  addPicture(cameraMode:boolean){
    console.log("addPicture");
    this._IMG.selectImage(cameraMode).then((image) => {
        //this._LOADER.displayPreloader('Procesando imagen.');
        this.photos.push(image);
        this.photosAdded = (this.photos.length > 0)? true : false;        
      }, (err) => {
      // Handle error 
        console.log("camera error: "+err);  
      }
    ).catch(error => {
       console.log("camera exception"+error);
    });
    this.photosAdded = (this.photos.length > 0)? true : false;
  }
  deletePhoto(index){
    let confirm = this.alertCtrl.create({
      title: 'Deseas eliminar la foto?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
            //console.log('Disagree clicked');
          }
        }, {
          text: 'Si',
          handler: () => {
            //console.log('Agree clicked');
            this.photos.splice(index, 1);
            this.photosAdded = (this.photos.length > 0)? true : false;
          }
        }
      ]
    });
    confirm.present();   
  }

  cancel(){
    //this.navCtrl.pop() doesnt wokr
    this.navCtrl.push(HomePage);
    return false;
  }

  static isChecked(control: FormControl) : any{    
    if(control.value != true){
      return {
        "notChecked" : true
      };
    }

    return null;
  }


}
