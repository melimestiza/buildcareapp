import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AngularFireDatabase,AngularFireList } from 'angularfire2/database';
import { AuthProvider } from '../../providers/auth/auth';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { WorksProvider } from '../../providers/works/works';
import { ListPage } from '../list/list';
import { LoginPage } from '../login/login';
import { AddWorkPage } from '../add-work/add-work';
import { WorkfinishPage } from '../workfinish/workfinish';
import { PicIniPage } from '../pic-ini/pic-ini';
import { PicFinishPage } from '../pic-finish/pic-finish';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public userwork:Object;
  homeExhibitions:any;
	public filteredExhibitions:any;
  public filterSubject: BehaviorSubject<string|null>;


  	constructor(public navCtrl: NavController,private db: AngularFireDatabase,private auth: AuthProvider,public fbp: FirebaseProvider,public work:WorksProvider) {
  		console.log(this.auth.uid);
  		 

  		this.filterSubject = new BehaviorSubject(null);
       
      //console.log(auth.user);
      

      this.auth.getUserData().subscribe(data => {
       console.log(data);
        if(data){
          console.log(data.uid);
        
        this.work.getWorks(data.uid).subscribe( dataObject => {
          this.userwork = dataObject;
          console.log(this.userwork);
        });
      
          this.homeExhibitions = HomePage;
        }else{        
          this.homeExhibitions = LoginPage;
        }
      });
	  }

    goToAdd(){
      this.navCtrl.push(AddWorkPage);
    }

    goToWorksEdit(dataObject) {    
      this.navCtrl.push(WorkfinishPage, dataObject);
    }
    goToPicStart(data) {    
      this.navCtrl.push(PicIniPage,data);
    }
    goToPicFinish(data) {    
      this.navCtrl.push(PicFinishPage,data);
    }


}
