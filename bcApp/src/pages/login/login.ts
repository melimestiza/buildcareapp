import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder,FormGroup,Validators } from '@angular/forms';

import { AuthProvider } from '../../providers/auth/auth';
import { Preloader } from '../../providers/preloader/preloader';

import { HomePage } from '../home/home';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    public form: FormGroup;
    error: any;



  constructor(public navCtrl: NavController, private _FB:FormBuilder,private auth:AuthProvider,private _LOADER: Preloader) {
      this.form = this._FB.group({
        'email': ['', Validators.required],
        'password': ['',Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
    
   /**
    * Log in using the loginWithEmailAndPassword method
    * from the AuthProvider service (supplying the email
    * and password FormControls from the template via the
    * FormBuilder object
    * @method logIn
    * @return {none}
    */

    login()
    {
      this._LOADER.displayPreloader('Verificando...');
      let email: any = this.form.controls['email'].value,
      password: any = this.form.controls['password'].value;
      email = email+"@buildcare.com";
      this.auth.loginUser(email,password ).then((user) => {
        this.navCtrl.setRoot(HomePage);
        this._LOADER.hidePreloader();
      })
       .catch(err=>{
         console.log(err);
        setTimeout(() => {
          this._LOADER.hidePreloader()
          this.error = err;
        }, 1000);
      })
    }
    

}
