import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PicFinishPage } from './pic-finish';

@NgModule({
  declarations: [
    PicFinishPage,
  ],
  imports: [
    IonicPageModule.forChild(PicFinishPage),
  ],
})
export class PicFinishPageModule {}
