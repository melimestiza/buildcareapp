import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PicIniPage } from './pic-ini';

@NgModule({
  declarations: [
    PicIniPage,
  ],
  imports: [
    IonicPageModule.forChild(PicIniPage),
  ],
})
export class PicIniPageModule {}
