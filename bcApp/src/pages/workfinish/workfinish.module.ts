import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkfinishPage } from './workfinish';

@NgModule({
  declarations: [
    WorkfinishPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkfinishPage),
  ],
})
export class WorkfinishPageModule {}
