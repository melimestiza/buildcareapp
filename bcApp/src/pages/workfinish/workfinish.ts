import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WorksProvider } from '../../providers/works/works';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Works,WorksStatus } from '../../model/works';
import { Preloader } from '../../providers/preloader/preloader';
import { HomePage } from '../home/home';


/**
 * Generated class for the WorkfinishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-workfinish',
  templateUrl: 'workfinish.html',
})
export class WorkfinishPage {
	process:any;
  public activities:Object;
  private activitiesForm:FormGroup;
  public elegidos:string[];
  private trabajos:Works;



  constructor(public work:WorksProvider,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder,private _loader:Preloader) {
  	this.process = navParams.data;
    this.elegidos = [];
    this.trabajos = new Works();

  	console.log(this.process);
    this.work.getAllActivities().subscribe( dataObject => {
      this.activities = dataObject;
    });

    this.activitiesForm = this.formBuilder.group({
      element: 'false',
    });

  }

  ionViewDidLoad() {
  }
  public notify(event:any,valor:string) {
    if (valor && event.checked == true) {
      this.elegidos.push(valor);
    }
    else
    {
      var index = this.elegidos.indexOf(valor);
      if (index > -1) {
        this.elegidos.splice(index,1);
      }
    }
  }
  private save(){
    console.log(this.elegidos);
    this.trabajos.actividad = this.elegidos;
    this.trabajos.id = this.process.key;
    this.trabajos.status = WorksStatus.STARTED.toString();       
    this._loader.displayPreloader('Procesando...');
      console.log(this.trabajos);        
      this.work.saveStart(this.trabajos).then(registerData => {
        console.log(registerData);
        this._loader.hidePreloader();
        this.cancel();
      }, registerError => {
        console.log(registerError);
        this._loader.hidePreloader();
      });
  }
  cancel(){
    //this.navCtrl.pop() doesnt wokr
    this.navCtrl.push(HomePage);
    return false;
  }


}
