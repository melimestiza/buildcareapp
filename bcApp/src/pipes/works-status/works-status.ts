import { Pipe, PipeTransform } from '@angular/core';
import { WorksStatus } from '../../model/works';

/**
 * Generated class for the ExhibitionStatusPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'worksStatus',
})
export class WorksStatusPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {  	
     let textStatus:String = "";
     let colorStatus:String = "";
     switch (parseInt(value)) {
      case WorksStatus.PENDING:
        colorStatus = "status-pending";
        textStatus = "En proceso";        
        break;
      case WorksStatus.ACCEPTED:        
        colorStatus = "status-accepted";
        textStatus = "Finalizada";
        break;
      case WorksStatus.STARTED:        
        colorStatus = "status-started";
        textStatus = "Seleccionado el trabajo";
        break;
      case WorksStatus.ONIT:
        colorStatus = "status-onit";
        textStatus = "Iniciado el trabajo";
        break;
      case WorksStatus.FINISH:
        colorStatus = "status-finish";
        textStatus = "Finalizado el trabajo";
        break;  
      default: //Unsync
        colorStatus = "status-unsync";
        textStatus = "Sin procesar";
        break;
     }
    
    if(args && args.length > 0 && args[0] == "color"){
      return colorStatus;
    }
    
    return textStatus;
  }
}
