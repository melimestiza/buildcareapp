import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable()
export class AuthProvider {
  public uid:string;
  user: Observable<firebase.User>;
  constructor(public http: Http,private afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
    // firebase.auth().onAuthStateChanged((user) =>
    //   {
    //     if (user)
    //     {

    //       console.log('User is signed in');
    //     }
    //     else
    //     {
    //       // No user is signed in.
    //       console.log('User is NOT signed in');
    //     }
    //   });
  }

  getUserData() {
    return Observable.create(observer => {
      this.user.subscribe(authData => {        
        if (authData) {                    
            this.uid = authData.uid;
            //Validar program
            //this.program = 1253;

            observer.next(authData);            
        } else {
          observer.next(null);
          //observer.error();          
        }
      });
    });
  }

    loginUser(email:string, password:string){
      return new Promise((resolve, reject) =>
      {
       this.afAuth.auth.signInWithEmailAndPassword(email, password).then((val:any) => 
       {
         this.uid = val.uid;
          resolve(val);
       })
      .catch((err : any) =>
        {
          switch (err.code) {
            case "auth/invalid-email":
              reject("El email no es correcto.");
            break;
            case "auth/user-not-found":
              reject("El usuario no existe, verifica tus datos.");
            break;
            case "auth/wrong-password":
              reject("La contraseña es incorrecta.");
            break; 
            case "auth/network-request-failed":
            reject("Verifique su conexión a internet.");       
            default:
              reject(err); 
            break;
          }
        });
      });
   }

    /**
    * Log out with Firebase Web API signOut method
    *
    * @method logOut
    * @return {Promise}
    */
   logOut(): Promise<any>
   {
      return new Promise((resolve, reject) =>
      {
        firebase.auth().signOut().then(() =>
        {
          console.log(this.uid);
           resolve(true);
        })
        .catch((error : any) =>
        {
           reject(error);
        });
      });
   }

   logout(){
     this.afAuth.auth.signOut().then(()=>{
       this.uid = '';
       // hemos salido
     })
   }
   // Devuelve la session
   get Session(){
    return this.afAuth.authState;
   }
    

}
