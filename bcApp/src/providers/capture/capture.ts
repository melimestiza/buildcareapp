import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Camera, CameraOptions } from '@ionic-native/camera';
/*
  Generated class for the MyCam provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CaptureProvider {

  public cameraImage : String;
  private cameraOptions : CameraOptions;



  constructor(private _CAMERA: Camera) {
    //console.log('Hello MyCam Provider');
     this.cameraOptions = {
             sourceType         : this._CAMERA.PictureSourceType.CAMERA,
             destinationType    : this._CAMERA.DestinationType.DATA_URL,
             quality            : 95,
             targetWidth        : 700,
             targetHeight       : 600,
             encodingType       : this._CAMERA.EncodingType.JPEG,
             correctOrientation : true,
             allowEdit          : false,
             cameraDirection    : this._CAMERA.Direction.FRONT
         };
  }

  selectImage(cameraMode:boolean) : Promise<any>
  {
      return new Promise(resolve =>
      {         
         if(cameraMode){
           this.cameraOptions.sourceType = this._CAMERA.PictureSourceType.CAMERA;
           this.cameraOptions.destinationType = this._CAMERA.DestinationType.DATA_URL;
         }
         else{
           this.cameraOptions.sourceType = this._CAMERA.PictureSourceType.SAVEDPHOTOALBUM;
           this.cameraOptions.destinationType = this._CAMERA.DestinationType.DATA_URL;
         }
         
         this._CAMERA.getPicture(this.cameraOptions)
         .then((data) =>
         {
            this.cameraImage   = "data:image/jpeg;base64," + data;
            resolve(this.cameraImage);
         }, (err) => {
            // Handle error
            //Promise.reject(err);
            console.log("error"+err);
         });


      });
  }
   
}
