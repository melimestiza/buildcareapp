import { Injectable } from '@angular/core';
import { AngularFireDatabase,AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';



/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

  constructor(public afd: AngularFireDatabase) {
  }

	list(path: string, filter?:any): Observable<any[]> {
	    return this.afd.list<Object>(path,filter).valueChanges();
	}
	object(path: string): Observable<any> {
     return this.afd.object(path).valueChanges();
  }  
  listRef(path: string, filter?:any): AngularFireList<any> {
    return this.afd.list<Object>(path,filter);
  }
  uploadImage(storagePath:string, dataToSave:string, filename:string): Promise<any>{
    let storageRef = firebase.storage().ref();    
    const imageRef = storageRef.child(`${storagePath}/${filename}.jpg`);
    return new Promise((resolve) => {      
      imageRef.putString(dataToSave, firebase.storage.StringFormat.DATA_URL).then((snapshot)=> {        
        resolve(snapshot);
      });
    }).catch(err => {
      console.log(err)
    });
  }
  save(path: string, data: any) : Promise<any>{
    data.created = firebase.database.ServerValue.TIMESTAMP;
    data.sortTime = -1 * Date.now();
    return this.afd.object(path).set(data);
  }
  update(path:string,data:any) : Promise<any>{
    return this.afd.object(path).update(data);
  }
  updateF(path:string,data:any) : Promise<any>{
    data.created = firebase.database.ServerValue.TIMESTAMP;
    return this.afd.object(path).update(data);
  }
}
