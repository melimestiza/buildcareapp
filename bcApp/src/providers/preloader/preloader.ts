import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

/*
  Generated class for the PreloaderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Preloader {
    private loading;any;

  constructor(public loadingCtrl: LoadingController) {
  }
  displayPreloader(messageLoader: string) : void
  {
      this.loading = this.loadingCtrl.create({
         content: messageLoader
      });

      this.loading.present();
  }
  hidePreloader() : void
  {
      this.loading.dismiss();
  }

}
