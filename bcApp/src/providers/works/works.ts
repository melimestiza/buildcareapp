import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AuthProvider } from '../auth/auth';
import { FirebaseProvider } from '../firebase/firebase';
import { Observable } from 'rxjs/Observable';

import { UUID } from 'angular2-uuid';


/*
  Generated class for the WorksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WorksProvider {

  constructor(public auth: AuthProvider,public data: FirebaseProvider) {
  }

  getWorks(id:String):Observable<any[]> {
    return this.data.listRef("/works/"+this.auth.uid+"/").snapshotChanges().map(changes => {                                 
      return changes.map(c => ({ key: c.payload.key, ... c.payload.val() }));
    });
	}
  getAllProperties() : Observable<any[]> {
    //console.log(this.auth.user); //validate active status
    return this.data.listRef("/property").snapshotChanges().map(changes => {            
      return changes.map(c => ({ key: c.payload.key, value: c.payload.val() }));
    });
  }
  getAllActivities() : Observable<any[]> {
    //console.log(this.auth.user); //validate active status
    return this.data.listRef("/apto").snapshotChanges().map(changes => {            
      return changes.map(c => ({ key: c.payload.key, value: c.payload.val() }));
    });
  }
  save(apto:any): Promise<any> {
    const dataToSave = [];
    for (var i = 0; i < apto.photos.length; ++i) {      
      //let photoFileName = Math.floor(Date.now() / 1000).toString();
      let photoFileName = UUID.UUID();      
      apto.photos.push(photoFileName);
      dataToSave.push(this.data.uploadImage("apartamentos",apto.photos[i],photoFileName).then( imageData => {        
        dataToSave.push(this.data.save("/photos-apto/"+photoFileName,{ "url": (imageData.downloadURL)? imageData.downloadURL : ""}));
      }));  
    }
    apto.uid = this.auth.uid;
    apto.id = UUID.UUID();
    dataToSave.push(this.data.save("/works/"+this.auth.uid+"/"+apto.id+"/",apto));
    return Promise.all(dataToSave);
  }
  saveStart(elementos:any){
    const datos = [];
    datos.push(this.data.update("/works/"+this.auth.uid+"/"+elementos.id+"/",elementos));
    return Promise.all(datos);
  }
  saveIniPics(pics:any){
    const datos = [];
    datos.push(this.data.update("/works/"+this.auth.uid+"/"+pics.id+"/",pics));
    return Promise.all(datos);
  }
  saveFinishPics(pics:any){
    const datos = [];
    datos.push(this.data.updateF("/works/"+this.auth.uid+"/"+pics.id+"/",pics));
    return Promise.all(datos);
  }
}
